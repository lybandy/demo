package cc.kd8;

import cc.kd8.dao.DbSession;
import cc.kd8.dao.UserDao;
import cc.kd8.entity.User;
import org.apache.ibatis.session.SqlSession;
import java.util.Date;
import java.util.List;
import org.junit.Test;
/**
 * @program: demo
 * @description: 用户的增删改查测试
 * @author: Andy
 * @create: 2018-08-09 15:56
 **/
public class UserTest {

    /**
     * 新增用户
     */
    @Test
    public   void insertUser() {
        SqlSession session =DbSession.getSession();
        UserDao mapper = session.getMapper(UserDao.class);
        Date time = new Date();
        User user = new User(123,"张三", "1314520", 20,time,time);
        try {
            mapper.insertUser(user);
            System.out.println(user.toString());
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        }
    }


//    /**
//     * 删除用户
//     */
//    @Test
//    public  void deleteUser(){
//        SqlSession session= DbSession.getSession();
//        UserDao mapper=session.getMapper(UserDao.class);
//        try {
//            mapper.deleteUser(1);
//            session.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            session.rollback();
//        }
//    }
//
//
//    /**
//     * 根据id查询用户
//     */
//    @Test
//    public  void selectUserById(){
//        SqlSession session=DbSession.getSession();
//        UserDao mapper=session.getMapper(UserDao.class);
//        try {
//            User user=    mapper.selectUserById(2);
//            System.out.println(user.toString());
//
//            session.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            session.rollback();
//        }
//    }
//
//    /**
//     * 查询所有的用户
//     */
//    @Test
//    public  void selectAllUser(){
//        SqlSession session=DbSession.getSession();
//        UserDao mapper=session.getMapper(UserDao.class);
//        try {
//            List<User> user=mapper.selectAllUser();
//            System.out.println(user.toString());
//            session.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            session.rollback();
//        }
//    }
}
