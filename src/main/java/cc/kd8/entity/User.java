package cc.kd8.entity;

import java.util.Date;

public class User {
    private Integer id;
    private String username;
    private String password;
    private Integer age;
    private Date create_time;
    private Date update_time;



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatetime() {
        return create_time;
    }

    public void setCreatetime(Date createtime) {
        this.create_time = createtime;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public User() {
        super();
    }

    public User(Integer id, String username, String password, Integer age,Date create_time,Date update_time) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.age = age;
        this.create_time=create_time;
        this.update_time=update_time;
    }
    public User( String username, String password, Integer age) {
        super();
        this.username = username;
        this.password = password;
        this.age = age;
    }
}
