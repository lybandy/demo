package cc.kd8.controller;

import cc.kd8.dao.DbSession;
import cc.kd8.dao.UserDao;
import cc.kd8.entity.User;
import org.apache.ibatis.session.SqlSession;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/user_create")
public class UserServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String displayTime = dtf.format(new Date());
        User user = insertUser();
        req.setAttribute("result",user.getUsername());
        req.getRequestDispatcher("/WEB-INF/jsp/user.jsp").forward(req,resp);
    }

    private User insertUser() {SqlSession session = DbSession.getSession();
        UserDao mapper = (UserDao)session.getMapper(UserDao.class);
        Date time = new Date();
        User user = new User(123, "张三", "1314520", 20, time, time);

        try {
            mapper.insertUser(user);
            System.out.println(user.toString());
            session.commit();
        } catch (Exception var6) {
            var6.printStackTrace();
            session.rollback();
        }
        return user;
    }

    public void getUserInfo(){
    }
}
